var onNavigate=function(e, initial){
	var toShow, toHide;

	if (location.hash=='#login'){
		toShow='logform';
		toHide='regform';
	}
	else if (location.hash=='#signin'){
		toShow='regform';
		toHide='logform';
	}

	if (!toShow) return;

	if (initial){
		document.getElementById(toShow).style.display='';
		document.getElementById(toHide).style.display='none';
		return;
	}

	$('#'+toHide).hide();
	$('#'+toShow).fadeIn(600);

	if (e) e.preventDefault();
};

window.addEventListener('hashchange', onNavigate);

$(function(){
	onNavigate(null, 'initial');
	$('select').material_select();
});
