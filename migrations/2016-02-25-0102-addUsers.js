'use strict';
var mongoose = require('lib/mongoose');
var User = require('models/user').User;

var users = [
	{firstname:'Albert', lastname:'Einstein', email:'ashtein@gmail.com', password:'123qwe'},
	{firstname:'Blaise', lastname:'Pascal', email:'pascal@yahoo.com', password:'qwe123'},
	{firstname:'Galileo', lastname:'Galilei', email:'gglei@hotmail.com', password:'ggglll'},
	{firstname:'Isaac', lastname:'Newton', email:'inew@gmail.com', password:'apple123'}
];

exports.up = function (next) {
	console.log('    --> This is migration 2016-02-25-0102-addUsers.js being applied');
	User.remove().exec(); // Clear collection
	mongoose.connection.collections.users.dropIndexes();
	User.create(users, function(err, docs){
		next(err);
	});
};


exports.down = function (next) {
	console.log('    --> This is migration 2016-02-25-0102-addUsers.js being rollbacked');
	User.remove().exec(); // Clear collection
	next();
};
