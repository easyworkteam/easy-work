var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var config = require('config');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('lib/mongoose');
var flash = require('connect-flash');
var view_helpers = require('view-helpers');
var helpers = require('express-helpers')();
var HttpError = require('error').HttpError;

var ECT = require('ect');
var ectRenderer = ECT({ watch: true, root: __dirname + '/views', ext : '.ect' });

var routes = require('./routes/index');
var users = require('./routes/users');
var tasks = require('./routes/tasks');
var pkg = require('package.json');

var app = express();

// view engine setup
app.set('view engine', 'ect');
app.engine('ect', ectRenderer.render);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// Statics
app.use(require('compression')());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components', express.static(path.join(__dirname, 'bower_components')));

app.use(logger('dev'));

app.use(cookieParser(config.get('session:secret')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// TODO: check session settings [Dima Sokol]
var MongoDBStore = require('connect-mongodb-session')(require('express-session'));
var store = new MongoDBStore({
	uri: config.get('mongoose:uri'),
	collection: 'sessions'
});

// Catch errors
store.on('error', function(error) {
	assert.ifError(error);
	assert.ok(false);
});

app.use(require('express-session')({
	name:'sid',
	resave:false,
	saveUninitialized:false,
	cookie: {
		maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
	},
	secret: config.get('session:secret'),
	store: store
}));

require('config/passport')(app);
app.use(flash());

// should be declared after session and flash
app.use(view_helpers(pkg.name));

// Static locals
app.locals.sitename=config.get('sitename');

// Helpers
app.locals.select_tag = helpers.select_tag;
app.locals.label_for = helpers.label_for;
app.locals.text_field_tag = helpers.text_field_tag;

// Routes
app.use('/', routes);
app.use('/users', users);
app.use('/tasks', tasks);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found!');
	err.status = 404;
	next(err);
});

// error handlers
if (app.get('env') === 'development') {
  app.set('json spaces', 2); // Make json reponses more readable in development
  // development error handler
  // will print stacktrace
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      title: 'Error',
      message: err.message,
      error: err
    });
  });
}
else {
  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      title: 'Error',
      message: err.message,
      error: {}
    });
  });
}

module.exports = app;
