var express = require('express');
var router = express.Router();
var User = require('models/user').User;

// TODO: Remove it for production [Dima Sokol]
router.get('/', function(req, res, next){
	if (req.app.get('env') === 'development'){
		User.find().exec(function(err, docs){
			if (err) next(err);

			res.render('users/list', { title: 'Users', users:docs });
		});
	}
	else {
		next();
	}
});

router.get('/:id', function(req, res, next){
	res.render('users/index', { title: 'Profile - '+req.user.fullname});
});
router.get('/edit/:id', function(req, res, next){
	res.render('users/edit', { title: 'Profile - '+req.user.fullname});
});

module.exports = router;
