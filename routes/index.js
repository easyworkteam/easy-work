var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('models/user').User;

// router.param('id', function(req, res, next, id) {
// 	User.findById(id, function(err, user) {
// 		if (err) {
// 			next(err);
// 		} else if (user) {
// 			req.user = user;
// 			next();
// 		} else {
// 			res.sendStatus('404');
// 			//next(new Error('failed to load user'));
// 		}
// 	});
// });

/* GET home page. */
router.get('/', function(req, res, next) {
	if (req.user){
		res.redirect('/tasks');
	}
	else {
		res.render('index', { title: 'Search tasks' }); // TODO: title ??
	}
});

router.get('/login/google', passport.authenticate('google', {
	promt: true,
	scope: ['profile', 'email']
}));

router.get('/login/google/callback', passport.authenticate('google', {
	successRedirect:'/',
	failureRedirect:'/#login',
	successFlash:'Welcome'
}));

// router.get('/login/vk', passport.authenticate('vkontakte'));
// router.get('/login/vk/callback', passport.authenticate('vkontakte', {
// 	successRedirect:'/',
// 	failureRedirect:'/#login',
// 	successFlash:'Welcome'
// }));

router.post('/login', passport.authenticate('local', {
	successRedirect:'/',
	failureRedirect:'/#login',
	successFlash:'Welcome',
	failureFlash:true
}));

router.get('/logout', function(req, res, next){
	req.logout();
	res.redirect('/');
});

router.post('/signin', function (req, res, next){
	var user = new User({
		firstname: req.body.fname,
		lastname: req.body.lname,
		password: req.body.pass,
		email: req.body.email
	});

	user.save(function (err) {
		if (!err) {
			req.flash('info', 'Registration successfull!');
			req.login(user, function(err) {
				if (err) { return next(err); }
				return res.redirect('/users/edit/'+user.id);
			});
		}
		else {
			console.log(err, user);
			res.render('index', {err:err, temp_user:user});
		}
	});
});

module.exports = router;
