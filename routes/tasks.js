var express = require('express');
var router = express.Router();
var Task = require('models/task').Task;

router.get('/new', function (req, res, next) {
	var task = new Task({});
	res.render('tasks/new', {task:task});
});

router.post('/create', function (req, res, next) {
	var task = new Task(req.body);
	task.save(function (err) {
		if (!err) {
			req.flash('info', 'Task created successfully!');
			res.redirect('/tasks');
		}
		else {
			console.log('err',err);
			res.render('tasks/new', {err:err, task:task});
		}
	});
});

router.param('id', function(req, res, next, id) {
	//console.log('id',id);
	Task.findOne({_id:id}, function(err, task) {
		if (err) {
			next(err);
		} else if (task) {
			req.task = task;
			console.log('task',task);
			next();
		} else {
			next(new Error('failed to load task'));
		}
	});
});

router.get('/', function (req, res, next) {
	const page = (req.query.page > 0 ? req.query.page : 1) - 1;
	const limit = 3;
	Task.count({}).exec()
	.then(function(count){
		return Task.find({}).skip(page*limit).limit(limit).exec('find')
			.then(function(tasks){
				return {count:count, tasks:tasks};
			});
	})
	.then(function(data){
		res.render('tasks/index', {
				pages: Math.ceil(data.count / limit),
				page: page + 1,
				tasks: data.tasks
			});
		},
		function(reason){
			console.log('Error',reason);
		}
	);
});

router.get('/:id', function (req, res, next) {
	res.render('tasks/view',{task:req.task});
});

router.post('/:id', function (req, res, next) {
	var task = req.task;
	console.log('task',task);
	task.title = req.body.title;
	task.price = req.body.price;
	task.description = req.body.description;
	task.save(function (err) {
		if (!err) {
			req.flash('info', 'Task updated successfully!');
			res.redirect('/tasks');
		}
		else {
			console.log('err',err);
			res.render('tasks/edit', {task:req.task, err:err});
		}
	});
});

router.get('/:id/edit', function (req, res, next) {
	res.render('tasks/edit',{task:req.task});
});

router.post('/:id/delete', function (req, res, next) {
	req.task.remove();
	req.flash('info', 'Task removed successfully!');
	res.redirect('/tasks');
});

module.exports = router;
