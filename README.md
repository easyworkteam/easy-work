# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone the Repo
* `npm install`
* `bower install`
* To use migrations: `npm install mongoose-data-migrate -g`

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

* Possible script for starting app
	- under Windows

	`echo OFF

	start "MongoDB Server" /MIN "PATH_TO_MONGO_BIN\mongod.exe" "--dbpath=PATH_TO_DB_FOLDER"
	set NODE_PATH=.;
	set NODE_ENV=development

	TITLE supervisor ./bin/www
	supervisor  --ignore _DBdata,.git ./bin/www
	echo ON`


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
