var async = require('async');
var mongoose = require('lib/mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    title: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category: {
        type: Number,
        required: true
    },
    price: {
        type: Number
    },
    photo: {
        type: String
    },
    urgent: {
        type: Boolean
    },
    deadline: {
        type: Date
    },
    created: {
        type: Date,
        default: Date.now
    }
});

exports.Task = mongoose.model('Task', schema);