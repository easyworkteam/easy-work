var crypto = require('crypto');
var async = require('async');
var util = require('util');

var mongoose = require('lib/mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
	firstname:{type:String, required:true},
	lastname:{type:String, required:true},
	extAuth: {
		google:String
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	hashedPassword: {
		type: String,
		required: true
	},
	salt: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

schema.methods.encryptPassword = function(password) {
	return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('fullname')
	.get(function(){
		return this.firstname + ' ' + this.lastname;
	});

schema.virtual('password')
	.set(function(password) {
		this._plainPassword = password;
		this.salt = Math.random() + '';
		this.hashedPassword = this.encryptPassword(password);
	})
	.get(function() { return this._plainPassword; });

// schema.path('password').validate([
// 	{validator:function(v){ return v.length>=5; }, message:'Password must be at least 6 symbols!'},
// 	{validator:function(v){ return /\d/.test(v) && /\w/.test(v); }, message:'Password must contain symbols and numbers!'},
// ]);
schema.path('email').validate(/[\w\d\.-_]+@[\w\d\.]+[\w\d]+/i, '{VALUE} is not a valid {PATH} address!');
schema.path('firstname').validate(/[\wа-яА-Я'-]+/i, 'Incorrect firstname!');
schema.path('lastname').validate(/[\wа-яА-Я'-]+/i, 'Incorrect lastname!');

schema.methods.checkPassword = function(password) {
	return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.authorize = function(email, password, callback) {
	var User = this;

	async.waterfall([
		function(callback) {
			User.findOne({email: email}, callback);
		},
		function(user, callback) {
			if (user) {
				if (user.checkPassword(password)) {
						callback(null, user);
				} else {
						callback(new AuthError("������ �������"));
				}
			} else {
				var user = new User({email: email, password: password});
				user.save(function(err) {
						if (err) return callback(err);
						callback(null, user);
				});
			}
		}
	], callback);
};


function AuthError(message) {
	Error.apply(this, arguments);
	Error.captureStackTrace(this, AuthError);

	this.message = message;
}

util.inherits(AuthError, Error);

AuthError.prototype.name = 'AuthError';

exports.User = mongoose.model('User', schema);
exports.AuthError = AuthError;
