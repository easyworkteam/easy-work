var User = require('models/user').User;
var VkStrategy = require('passport-vkontakte').Strategy;
var config = require('config');

module.exports = function (passport) {
	passport.use(new VkStrategy({
		clientID:config.get('extAuth:vk:clientID'),
		clientSecret:config.get('extAuth:vk:clientSecret'),
		callbackURL:config.get('extAuth:vk:callbackUrl')
	},
	function(accessToken, refreshToken, profile, done){
		User.findOne({ 'extAuth.vkontakte':profile.id }, function(err, user){
			if (err) return done(err);
			if (!user){
				console.log(profile);
				done(null, null, 'Problem');
				return;
				User.findOne({ 'email':profile.emails[0].value }, function(err, user){
					if (err) return done(err);
					if (!user){
						User.create({
							firstname:profile.name.givenName,
							lastname:profile.name.familyName,
							email:profile.emails[0].value,
							extAuth:{
								vkontakte:profile.id
							}
						}, done);
					}
					
					done(null, null, 'User already exists with '+profile.emails[0].value+'email!');
				})
				return;
			}
			
			return done(null, user);
		});
	}));
};
