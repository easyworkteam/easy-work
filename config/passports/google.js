var crypto = require('crypto');
var User = require('models/user').User;
var Google2Strategy = require('passport-google-oauth').OAuth2Strategy;
var config = require('config');

module.exports = function (passport) {
	passport.use(new Google2Strategy({
		clientID:config.get('extAuth:google:clientID'),
		clientSecret:config.get('extAuth:google:clientSecret'),
		callbackURL:config.get('extAuth:google:callbackUrl')
	},
	function(accessToken, refreshToken, profile, done){
		User.findOne({ 'extAuth.google':profile.id }, function(err, user){
			if (err) return done(err);
			if (user) return done(null, user); // User found - login

			// User not found - create
			User.findOne({ email:profile.emails[0].value }, function(err, user){
				if (err) done(err);
				if (!user) {
					User.create({
						firstname:profile.name.givenName,
						lastname:profile.name.familyName,
						email:profile.emails[0].value,
						password:crypto.createHmac('sha1', Math.random()+'').update(profile.id+Math.random()).digest('hex'),
						extAuth:{
							google:profile.id
						}
					}, done);
					return;
				}
				console.log('Can\'t  create user with same email');
				done(null, null, 'User already exists with "'+profile.emails[0].value+'" email!');
			});
		});
	}));
};
