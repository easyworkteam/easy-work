var User = require('models/user').User;
var LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport) {
	passport.use(new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password'
	},
	function(email, password, done){
		User.findOne({ email: email }, function(err, user) {
			if (err) { return done(err); }
			if (!user) {
				return done(null, false, { message: 'Incorrect email.' });
			}
			// TODO:remove with something more reliable
			if (user.extAuth){
				for (var provider in user.extAuth) {
					if (user.extAuth[provider]) return done(null, false, { message: 'User can\'t be logged since he is "connected" user' });
				}
			}

			if (!user.checkPassword(password)) {
				return done(null, false, { message: 'Incorrect password.' });
			}

			return done(null, user);
		});
	}));
};
