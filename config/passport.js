var User = require('models/user').User;
var passport = require('passport');

// TODO: Make some more reliable serializations [Dima Sokol]
passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	User.findById(id, function(err, user){
		if (err) return done(err);
		if (!user) return done(null, false);
		done(null, user);
	});
});

// Require strategies
require('./passports/local')(passport);
require('./passports/google')(passport);
// require('./passports/vk')(passport); - vk does not provide email
// ---

module.exports = function (app){
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(function(req, res, next){
		res.locals.user=req.user;
		next();
	});
}
